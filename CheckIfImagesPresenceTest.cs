﻿using System;
using System.Threading;
using CheckIfImagePresence.PageObjects;
using MainPage.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CheckIfImagesPresenceTest
{
    public class Tests
    {
        private IWebDriver _webDriver;
        public static string googleLink = "https://www.google.com.ua";

        [SetUp]
        public void Setup()
        {
            _webDriver = new ChromeDriver();
            _webDriver.Navigate().GoToUrl(googleLink);
            _webDriver.Manage().Window.Maximize();
        }

        [Test]
        public void CheckIfImagesPresenceTest()
        {
            var searchExecute = new SearchPagePageObject(_webDriver);
            searchExecute.SearchTheImages();

            var checkExecute = new SearchResultsPageObject(_webDriver);
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);

            Assert.IsTrue(checkExecute.CheckIfImagesPresence());
        }

        [TearDown]
        public void TearDown()
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            _webDriver.Quit();
           
        }
    }
}
