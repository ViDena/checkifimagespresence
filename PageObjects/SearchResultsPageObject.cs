﻿using System;
using OpenQA.Selenium;

namespace CheckIfImagePresence.PageObjects
{
	public class SearchResultsPageObject
    {
		private IWebDriver _webDriver;
        private readonly string _imageItem = "//img[contains(@id, 'img')]";

        public SearchResultsPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public bool CheckIfImagesPresence()
        {
            try
            {
                // create a set of pictures and check their the number
                var imagesItemsSet = _webDriver.FindElements(By.XPath(_imageItem)).Count;
                if (imagesItemsSet >= 2) return true;
                else return false;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}

