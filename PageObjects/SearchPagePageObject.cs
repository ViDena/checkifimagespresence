﻿using System;
using System.Threading;
using CheckIfImagePresence.PageObjects;
using OpenQA.Selenium;

namespace MainPage.PageObjects
{
    public class SearchPagePageObject
    {
        private IWebDriver _webDriver;

        private static string searchFieldXpass = "//input[@title='Поиск']";
        private static string searchbuttonXpass = "//input[@value='Поиск в Google']";

        private readonly By _searchField = By.XPath(searchFieldXpass);
        private readonly By _searchButton = By.XPath(searchbuttonXpass);

        public static string textForSearchRequest = "зображення";

        public SearchPagePageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public SearchResultsPageObject SearchTheImages()
        {
            _webDriver.FindElement(_searchField).SendKeys(textForSearchRequest);
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            _webDriver.FindElement(_searchButton).Click();

            return new SearchResultsPageObject(_webDriver);
        }
    }
}

